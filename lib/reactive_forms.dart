import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

class ReactiveForms extends StatefulWidget {
  const ReactiveForms({Key? key}) : super(key: key);

  @override
  State<ReactiveForms> createState() => _ReactiveFormsState();
}

class _ReactiveFormsState extends State<ReactiveForms> {
// specifications category list
  List<DropdownMenuItem> specificationsItemsList = [
    DropdownMenuItem(value: 'Poids', child: Text('Poids')),
    DropdownMenuItem(value: 'Matière', child: Text('Matière')),
    DropdownMenuItem(value: 'Couleur', child: Text('Couleur')),
    DropdownMenuItem(value: 'Dimension', child: Text('Dimension')),
    DropdownMenuItem(value: 'Etat', child: Text('Etat')),
  ];

  // ==========================================================================
  // REACTIVE FORMS
  // ==========================================================================
  final form = FormGroup({
    'specifications': FormArray([]),
  });

  // define a getter
  late FormArray specifications = form.control('specifications') as FormArray;

  // add nested form array
  void _addNewSpecs() {
    specifications.add(
      FormGroup({
        'description': FormControl<String>(),
      }),
    );
  }

  // ==========================================================================
  // LIFECYCLE EVENTS
  // ==========================================================================
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ReactiveForm(
      formGroup: form,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Reactive forms"),
          actions: [],
        ),
        backgroundColor: Colors.white,
        body: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          onPressed: () {
                            _addNewSpecs();
                          },
                          child: Text(
                            "+ AJOUTER UNE SPECIFICATION",
                            style: TextStyle(
                              fontSize: 10,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ReactiveFormArray(
                  formArray: specifications,
                  builder: (
                    BuildContext context,
                    FormArray<Object?> formArray,
                    Widget? child,
                  ) {
                    final specs = formArray.controls
                        .map((control) => control as FormGroup)
                        .map(
                      (form) {
                        return ReactiveFormBuilder(
                          form: () => form,
                          builder: ((context, formGroup, child) {
                            return Row(
                              children: [
                                Expanded(
                                  flex: 3,
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    child: ReactiveTextField(
                                      formControlName: 'description',
                                      decoration: InputDecoration(
                                        hintText: "Spécification",
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: ReactiveFormConsumer(
                                    builder: (context, formGroup, child) {
                                      return IconButton(
                                        onPressed: () {
                                          formArray.remove(form);
                                        },
                                        icon: Icon(Icons.close),
                                        color: Colors.red,
                                      );
                                    },
                                  ),
                                ),
                              ],
                            );
                          }),
                        );
                      },
                    );

                    return Column(
                      verticalDirection: VerticalDirection.up,
                      children: specs.toList(),
                    );
                  },
                ),
              ],
            ),
            SizedBox(height: 100),
          ],
        ),
      ),
    );
  }
}
