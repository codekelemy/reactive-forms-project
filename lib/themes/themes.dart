import 'package:flutter/material.dart';

// colors scheme
class UIColors {
  // static colors
  static const Color primary = Color(0xFF6B2E88);
  static const Color primaryLight = Color(0xFFF6ECF8);
  static const Color secondary = Color(0xFF000000);
  static const Color accent = Color(0xFFC769D1);
  static const Color alert = Color(0xFFD53D44);
  static const Color positive = Color(0xFF5FB82C);
  static const Color positiveDark = Color.fromARGB(255, 50, 115, 12);
  static const Color info = Color.fromARGB(255, 41, 190, 249);
  static const Color warning = Color(0xFFFFC600);
  static const Color white = Color(0xFFFFFFFF);
  static const Color grey = Color(0xFFF2F2F2);
  static const Color lightGrey = Color.fromARGB(255, 174, 173, 173);
  static const Color darkGrey = Color(0xFF787878);
  static const Color none = Colors.transparent;
}

// gradients scheme
class UIGradients {
  // gradients colors
  static const LinearGradient primary = LinearGradient(
    colors: [
      UIColors.accent,
      UIColors.primary,
    ],
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
  );

  static const LinearGradient secondary = LinearGradient(
    colors: [
      UIColors.darkGrey,
      UIColors.grey,
    ],
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
  );

  static const LinearGradient alert = LinearGradient(
    colors: [
      UIColors.warning,
      UIColors.alert,
    ],
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
  );

  static const LinearGradient positive = LinearGradient(
    colors: [
      UIColors.positive,
      Color.fromARGB(255, 49, 118, 9),
    ],
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
  );

  static const LinearGradient info = LinearGradient(
    colors: [
      UIColors.info,
      Color.fromARGB(255, 9, 58, 118),
    ],
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
  );

  static const LinearGradient negative = LinearGradient(
    colors: [
      UIColors.alert,
      Color.fromARGB(255, 145, 40, 46),
    ],
    begin: FractionalOffset.centerLeft,
    end: FractionalOffset.centerRight,
  );
}

// forms
class UIForms {
  // textfield label

  static Widget textFieldLabel({String name = "Monetiz label"}) {
    return Text(
      name,
      style: TextStyle(
        color: UIColors.primary,
        fontSize: 12,
      ),
    );
  }

  // button style
  static ButtonStyle button({
    Color color = UIColors.white,
    Color background = UIColors.primary,
    Size minimumSize = const Size(50, 40),
    double borderRadius = 50,
  }) {
    return ElevatedButton.styleFrom(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      side: BorderSide(
        style: BorderStyle.none,
      ),
      onPrimary: color,
      primary: background,
      shadowColor: Colors.transparent,
      minimumSize: minimumSize,
    );
  }

  // textfield style
  static InputDecoration textField({
    Icon? icon,
    Color color = UIColors.primaryLight,
    Color textColor = UIColors.primary,
    String? labelText,
    String? hintText,
    double fontSize = 14,
    dynamic suffixIcon,
    EdgeInsetsGeometry contentPadding = const EdgeInsets.all(15),
  }) {
    return InputDecoration(
      prefixIcon: icon,
      prefixIconColor: textColor,
      prefixIconConstraints: BoxConstraints.tight(Size(30, 15)),
      suffixIcon: suffixIcon,
      suffixIconColor: textColor,
      suffixIconConstraints: BoxConstraints.tightFor(),
      contentPadding: contentPadding,
      isDense: true,
      hoverColor: UIColors.white,
      floatingLabelBehavior: FloatingLabelBehavior.never,
      fillColor: color,
      filled: true,
      labelText: labelText,
      hintText: hintText,
      hintStyle: TextStyle(
        color: textColor,
        fontSize: fontSize,
        decoration: TextDecoration.none,
      ),
      labelStyle: TextStyle(
        color: textColor,
        fontSize: fontSize,
        decoration: TextDecoration.none,
      ),
      errorStyle: TextStyle(
        color: UIColors.alert,
        fontSize: 12,
        decoration: TextDecoration.none,
      ),
      errorMaxLines: 1,
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(5),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: UIColors.primary,
          width: 1,
        ),
      ),
    );
  }

  // textfield miltilines style
  static InputDecoration textFieldMultiline({
    Color color = UIColors.primaryLight,
    Color textColor = UIColors.primary,
    bool bordered = false,
    String? labelText,
    String? hintText,
    double fontSize = 14,
    EdgeInsetsGeometry contentPadding = const EdgeInsets.all(0),
  }) {
    return InputDecoration(
      // prefixIconConstraints: BoxConstraints.tight(Size(30, 15)),
      constraints: BoxConstraints.expand(),
      contentPadding: contentPadding,
      // isDense: true,
      hoverColor: UIColors.white,
      floatingLabelBehavior: FloatingLabelBehavior.never,
      fillColor: color,
      filled: true,
      labelText: labelText,
      hintText: hintText,
      alignLabelWithHint: true,
      hintMaxLines: 5,
      hintStyle: TextStyle(
        color: textColor,
        fontSize: fontSize,
        decoration: TextDecoration.none,
      ),
      labelStyle: TextStyle(
        color: textColor,
        fontSize: fontSize,
        decoration: TextDecoration.none,
      ),
      errorStyle: TextStyle(
        color: UIColors.alert,
        fontSize: fontSize,
        decoration: TextDecoration.none,
      ),
      helperStyle: TextStyle(
        color: UIColors.primary,
        fontSize: fontSize,
        decoration: TextDecoration.none,
      ),
      errorMaxLines: 1,
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(5),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: bordered == true ? UIColors.primary : UIColors.none,
          width: 1,
        ),
      ),
    );
  }

  // dropdown textfield style
  static InputDecoration textFieldDropdown({
    Color color = UIColors.primaryLight,
    String? labelText,
    Color textColor = UIColors.primary,
    EdgeInsetsGeometry contentPadding = const EdgeInsets.all(10),
  }) {
    return InputDecoration(
      contentPadding: contentPadding,
      isDense: true,
      hoverColor: UIColors.white,
      floatingLabelBehavior: FloatingLabelBehavior.never,
      fillColor: color,
      filled: true,
      labelText: labelText,
      hintText: labelText,
      hintStyle: TextStyle(
        color: textColor,
        fontSize: 14,
      ),
      labelStyle: TextStyle(
        color: textColor,
        fontSize: 14,
      ),
      errorStyle: TextStyle(
        color: UIColors.alert,
        fontSize: 14,
      ),
      errorMaxLines: 1,
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.circular(5),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: UIColors.primary,
          width: 2,
        ),
      ),
    );
  }
}

// utilities
class UIUtils {
  static BoxShadow boxShadow = BoxShadow(
    color: Color.fromARGB(19, 0, 0, 0),
    offset: Offset(0, 1),
    blurRadius: 10,
    spreadRadius: 5,
  );
}
