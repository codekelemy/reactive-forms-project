import 'package:flutter/material.dart';

// categories
List<DropdownMenuItem> categoriesItemsList = [
  DropdownMenuItem(value: 'Robe', child: Text('Robe')),
  DropdownMenuItem(value: 'Boubou', child: Text('Boubou')),
  DropdownMenuItem(value: 'Sac à main', child: Text('Sac à main')),
  DropdownMenuItem(value: 'Chaussure', child: Text('Chaussure')),
  DropdownMenuItem(value: 'Veste', child: Text('Veste')),
  DropdownMenuItem(value: 'Nuisette', child: Text('Nuisette')),
  DropdownMenuItem(value: 'Accessoires', child: Text('Accessoires')),
  DropdownMenuItem(value: 'Bracelet', child: Text('Bracelet')),
  DropdownMenuItem(value: 'Polos', child: Text('Polos')),
  DropdownMenuItem(value: 'Jeans', child: Text('Jeans')),
];

// categories items list of string
List<String> categoriesItemsListString = [
  'Robe',
  'Boubou',
  'Sac à main',
  'Chaussure',
  'Veste',
  'Nuisette',
  'Accessoires',
  'Polos',
  'Jeans',
];

// specifications
List<DropdownMenuItem> specificationsItemsList = [
  DropdownMenuItem(value: 'Poids', child: Text('Poids')),
  DropdownMenuItem(value: 'Matière', child: Text('Matière')),
  DropdownMenuItem(value: 'Couleur', child: Text('Couleur')),
  DropdownMenuItem(value: 'Dimension', child: Text('Dimension')),
  DropdownMenuItem(value: 'Etat', child: Text('Etat')),
];

// specifications
List<DropdownMenuItem> citiesItemsList = [
  DropdownMenuItem(value: 'Cocody', child: Text('Cocody')),
  DropdownMenuItem(value: 'Koumassi', child: Text('Koumassi')),
  DropdownMenuItem(value: 'Treichville', child: Text('Treichville')),
  DropdownMenuItem(value: 'Adjamé', child: Text('Adjamé')),
];
