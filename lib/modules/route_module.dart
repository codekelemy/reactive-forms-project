import 'package:flutter_modular/flutter_modular.dart';
import 'package:reactiveformproject/reactive_forms.dart';

class RouteModule extends Module {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRoute> get routes => [
        // launch page
        ChildRoute(
          '/',
          child: (context, args) => ReactiveForms(),
          guards: [],
        ),
      ];
}
